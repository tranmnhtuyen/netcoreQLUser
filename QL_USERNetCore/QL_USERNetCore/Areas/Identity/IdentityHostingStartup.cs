﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using QL_USERNetCore.Areas.Identity.Data;
using QL_USERNetCore.Data;

[assembly: HostingStartup(typeof(QL_USERNetCore.Areas.Identity.IdentityHostingStartup))]
namespace QL_USERNetCore.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<LoginContext>(options =>
                    options.UseSqlite(
                        context.Configuration.GetConnectionString("LoginContextConnection")));

                services.AddDefaultIdentity<QL_USERNetCoreUser>(options => options.SignIn.RequireConfirmedAccount = true)
                    .AddEntityFrameworkStores<LoginContext>();
            });
        }
    }
}