#pragma checksum "F:\CTYCUONGKHANG\NetCore\QL_USERNetCore\QL_USERNetCore\Views\HistoryLogin\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "9414c9d5fe643118fad50f0ea227f755d9e1f7c2"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_HistoryLogin_Index), @"mvc.1.0.view", @"/Views/HistoryLogin/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "F:\CTYCUONGKHANG\NetCore\QL_USERNetCore\QL_USERNetCore\Views\_ViewImports.cshtml"
using QL_USERNetCore;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "F:\CTYCUONGKHANG\NetCore\QL_USERNetCore\QL_USERNetCore\Views\HistoryLogin\Index.cshtml"
using X.PagedList.Mvc.Core;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "F:\CTYCUONGKHANG\NetCore\QL_USERNetCore\QL_USERNetCore\Views\HistoryLogin\Index.cshtml"
using X.PagedList;

#line default
#line hidden
#nullable disable
#nullable restore
#line 19 "F:\CTYCUONGKHANG\NetCore\QL_USERNetCore\QL_USERNetCore\Views\HistoryLogin\Index.cshtml"
using X.PagedList.Mvc.Core.Fluent;

#line default
#line hidden
#nullable disable
#nullable restore
#line 21 "F:\CTYCUONGKHANG\NetCore\QL_USERNetCore\QL_USERNetCore\Views\HistoryLogin\Index.cshtml"
using QL_USERNetCore.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"9414c9d5fe643118fad50f0ea227f755d9e1f7c2", @"/Views/HistoryLogin/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"040323b7c58c482af799fb6397871d214823d123", @"/_ViewImports.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"040323b7c58c482af799fb6397871d214823d123", @"/Views/_ViewImports.cshtml")]
    public class Views_HistoryLogin_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<X.PagedList.IPagedList<QL_USERNetCore.Models.HistoryLogin>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 2 "F:\CTYCUONGKHANG\NetCore\QL_USERNetCore\QL_USERNetCore\Views\HistoryLogin\Index.cshtml"
  
    ViewData["Title"] = "Index";
    

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "F:\CTYCUONGKHANG\NetCore\QL_USERNetCore\QL_USERNetCore\Views\HistoryLogin\Index.cshtml"
       
    //Layout = "~/Views/User/Index.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n");
            WriteLiteral("\r\n");
            WriteLiteral("\r\n");
            WriteLiteral("\r\n");
            WriteLiteral("\r\n");
            WriteLiteral("\r\n<table class=\"table\">\r\n    <tr>\r\n        <th>ID</th>\r\n        <th>Code</th>\r\n        <th>LoginDate</th>\r\n        <th>LogOutDate</th>\r\n    </tr>\r\n");
#nullable restore
#line 30 "F:\CTYCUONGKHANG\NetCore\QL_USERNetCore\QL_USERNetCore\Views\HistoryLogin\Index.cshtml"
     foreach (var item in Model)
    {

#line default
#line hidden
#nullable disable
            WriteLiteral("        <tr>\r\n            <td>");
#nullable restore
#line 33 "F:\CTYCUONGKHANG\NetCore\QL_USERNetCore\QL_USERNetCore\Views\HistoryLogin\Index.cshtml"
           Write(item.Id);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n            <td>");
#nullable restore
#line 34 "F:\CTYCUONGKHANG\NetCore\QL_USERNetCore\QL_USERNetCore\Views\HistoryLogin\Index.cshtml"
           Write(item.Code);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n            <td>");
#nullable restore
#line 35 "F:\CTYCUONGKHANG\NetCore\QL_USERNetCore\QL_USERNetCore\Views\HistoryLogin\Index.cshtml"
           Write(item.LoginDate);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n            <td>");
#nullable restore
#line 36 "F:\CTYCUONGKHANG\NetCore\QL_USERNetCore\QL_USERNetCore\Views\HistoryLogin\Index.cshtml"
           Write(item.LogoutDate);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n        </tr>\r\n");
#nullable restore
#line 38 "F:\CTYCUONGKHANG\NetCore\QL_USERNetCore\QL_USERNetCore\Views\HistoryLogin\Index.cshtml"
    }

#line default
#line hidden
#nullable disable
            WriteLiteral("</table>\r\n<div>\r\n    <br />\r\n    Page ");
#nullable restore
#line 42 "F:\CTYCUONGKHANG\NetCore\QL_USERNetCore\QL_USERNetCore\Views\HistoryLogin\Index.cshtml"
     Write(Model.PageCount < Model.PageNumber ? 0 : Model.PageNumber);

#line default
#line hidden
#nullable disable
            WriteLiteral(" / ");
#nullable restore
#line 42 "F:\CTYCUONGKHANG\NetCore\QL_USERNetCore\QL_USERNetCore\Views\HistoryLogin\Index.cshtml"
                                                                   Write(Model.PageCount);

#line default
#line hidden
#nullable disable
            WriteLiteral(")\r\n    <ul class=\"pagination\">\r\n        <li class=\"page-item\">\r\n            ");
#nullable restore
#line 45 "F:\CTYCUONGKHANG\NetCore\QL_USERNetCore\QL_USERNetCore\Views\HistoryLogin\Index.cshtml"
       Write(Html.PagedListPager(Model, page => Url.Action("Index",
new { page })));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </li>\r\n\r\n    </ul>\r\n</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<X.PagedList.IPagedList<QL_USERNetCore.Models.HistoryLogin>> Html { get; private set; }
    }
}
#pragma warning restore 1591
