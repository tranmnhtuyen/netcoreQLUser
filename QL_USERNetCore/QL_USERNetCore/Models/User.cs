﻿using System;
using System.Collections.Generic;

namespace QL_USERNetCore.Models
{
    public partial class User
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string InsertUserId { get; set; }
        public string InsertDate { get; set; }
        public string LastEditUser { get; set; }
        public string LastEditDate { get; set; }
    }
}
