﻿using System;
using System.Collections.Generic;

namespace QL_USERNetCore.Models
{
    public partial class HistoryLogin
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string LoginDate { get; set; }
        public string LogoutDate { get; set; }
    }
}
