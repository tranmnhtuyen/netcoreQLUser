﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace QL_USERNetCore.Controllers
{
    using X.PagedList;
    using QL_USERNetCore.Models;
    using OfficeOpenXml;
    using System.Data;
    using System.Reflection;
    using ClosedXML.Excel;
    using System.IO;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.Data.SqlClient;
    using System.Configuration;
    using Microsoft.VisualBasic;
    using DocumentFormat.OpenXml.Math;
    using DocumentFormat.OpenXml.InkML;
    using DocumentFormat.OpenXml.ExtendedProperties;
    using DocumentFormat.OpenXml.Spreadsheet;

    //[Authorize]
    public class UserController : Controller
    {
        private readonly DB_USERContext dB_USERContext;
        private readonly IConfiguration _configuration;
        public UserController(IConfiguration configuration)
        {
            _configuration = configuration;

        }
        public IActionResult Index(int? page)
        {
            ViewBag.Session = HttpContext.Session.GetString("Code");
            if (ViewBag.Session == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                ExportExcel();

                if (page == null) page = 1;

             
                //-----------------------------get data ---------------------------
                string ConnectionString = _configuration.GetConnectionString("DBConnectString");
                SqlConnection connection = new SqlConnection(ConnectionString);
                connection.Open();
                string SqlQuery = "exec GetUser";
                SqlCommand command = connection.CreateCommand();
                command.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter(SqlQuery, connection);
                DataTable dataTable = new DataTable();

                int link = da.Fill(dataTable);
                // DataTable to List
                List<User> UserList = new List<User>();
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    User user = new User();
                    user.Id = Convert.ToInt32(dataTable.Rows[i]["Id"]);
                    user.Code = dataTable.Rows[i]["Code"].ToString();
                    user.Name = dataTable.Rows[i]["Name"].ToString();
                    user.Password = dataTable.Rows[i]["Password"].ToString();
                    user.InsertUserId = dataTable.Rows[i]["InsertUserId"].ToString();
                    user.InsertDate = dataTable.Rows[i]["InsertDate"].ToString();
                    user.LastEditUser = dataTable.Rows[i]["LastEditUser"].ToString();
                    user.LastEditDate = dataTable.Rows[i]["LastEditDate"].ToString();
                    UserList.Add(user);
                }
                int pageSize = 10;

                var links = UserList.ToPagedList(page ?? 1, pageSize);

                int pageNumber = (page ?? 1);

                return View(links.ToPagedList(pageNumber, pageSize));
            }
        }


        public IActionResult CreateUser(QL_USERNetCore.Models.User model)
        {
            ViewBag.Session = HttpContext.Session.GetString("Code");
            if (ViewBag.Session == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                if (model.Code == null || model.Password == null)
                {
                    ViewBag.CreateUserErro = "Code và Password không được bỏ trống!";
                    return View("CreateUser", model);
                }
                else
                {
                    string ConnectionString = _configuration.GetConnectionString("DBConnectString");
                    SqlConnection connection = new SqlConnection(ConnectionString);
                    connection.Open();
                    SqlCommand cmd2 = connection.CreateCommand();
                    cmd2.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd2.CommandText = "TimUserCode";
                    cmd2.Parameters.AddWithValue("@Code", model.Code);
                    int UserRecord = Convert.ToInt32(cmd2.ExecuteScalar());
                    connection.Close();
                    if (UserRecord == 0)
                    {
                        connection.Open();
                        SqlCommand cmd3 = connection.CreateCommand();
                        cmd3.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd3.CommandText = "inser_user";
                        cmd3.Parameters.AddWithValue("@Code", model.Code);
                        cmd3.Parameters.AddWithValue("@Name", model.Name);
                        cmd3.Parameters.AddWithValue("@Password", model.Password);
                        cmd3.Parameters.AddWithValue("@InsertUserId", HttpContext.Session.GetString("Code"));
                        cmd3.Parameters.AddWithValue("@InsertDate", DateTime.Now.ToString());

                        cmd3.ExecuteScalar();
                        TempData["CreateUserError"] = "Thêm thành công!";
                        return View("CreateUser", model);
                    }
                    else
                    {
                        TempData["CreateUserError"] = "Code đã được sử dụng vui lòng chọn Code khác!";
                        return View("CreateUser", model);
                    }
                    var r = "";

                    //var InsertDate = DateTime.Now.ToString();
                    //var InsertUserId = HttpContext.Session.GetString("Code");
                    //dB_USERContext.User.Add(new User() { Code = model.Code, Name = model.Name, Password = model.Password, InsertUserId = InsertUserId, InsertDate = InsertDate });
                    //dB_USERContext.SaveChanges();
                    //ViewBag.CreateUser = "Thêm tài khoản thành công!";
                    //TempData["CreateUser"] = "Thêm tài khoản thành công!";
                    return View("CreateUser", model);
                }
            }
        }


        public IActionResult EditUser(int idUser)
        {
            ViewBag.Session = HttpContext.Session.GetString("Code");
            if (ViewBag.Session == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                string ConnectionString = _configuration.GetConnectionString("DBConnectString");
                SqlConnection connection = new SqlConnection(ConnectionString);
                SqlCommand command = connection.CreateCommand();

                connection.Open();
                string SqlQuery = "exec GetDataEdit @ID";

                command.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter(SqlQuery, connection);
                da.SelectCommand.Parameters.AddWithValue("@ID", Convert.ToInt32(idUser));
                DataTable dataTable = new DataTable();

                int UserDetail = da.Fill(dataTable);

                var user = new User();
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {

                    user.Id = Convert.ToInt32(dataTable.Rows[i]["Id"]);
                    user.Code = dataTable.Rows[i]["Code"].ToString();
                    user.Name = dataTable.Rows[i]["Name"].ToString();
                    user.Password = dataTable.Rows[i]["Password"].ToString();
                    user.LastEditUser = dataTable.Rows[i]["LastEditUser"].ToString();
                    user.LastEditDate = dataTable.Rows[i]["LastEditDate"].ToString();
                }
                return View("EditUser", user);
            }
        }

        [HttpPost]
        public IActionResult EditUser(User user)
        {
            ViewBag.Session = HttpContext.Session.GetString("Code");
            if (ViewBag.Session == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                string ConnectionString = _configuration.GetConnectionString("DBConnectString");
                SqlConnection connection = new SqlConnection(ConnectionString);
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "TimUser";
                cmd.Parameters.AddWithValue("@ID", user.Id);
                int UserRecord = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();
                if (UserRecord == 1)
                {
                    connection.Open();
                    SqlCommand cmd1 = connection.CreateCommand();

                    cmd1.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd1.CommandText = "EditUser";
                    cmd1.Parameters.AddWithValue("@ID", Convert.ToInt32(user.Id));
                    cmd1.Parameters.AddWithValue("@Code", user.Code);
                    cmd1.Parameters.AddWithValue("@Name", user.Name);
                    cmd1.Parameters.AddWithValue("@Password", user.Password);
                    cmd1.Parameters.AddWithValue("@LastEditUser", HttpContext.Session.GetString("Code"));
                    cmd1.Parameters.AddWithValue("@LastEditDate", DateTime.Now.ToString());
                    cmd1.ExecuteScalar();
                    connection.Close();
                    TempData["EditMessage"] = "Sửa thành công! " + user.Code;
                    //var UserRecords = dB_USERContext.User.ToList();
                    return View("EditUser", user);
                }
                else
                {
                    TempData["EditMessageEr"] = "Bạn không được để trống  Password !";
                    //var UserRecords = dB_USERContext.User.ToList();
                    return View("EditUser", user);
                }
            }
        }

        public IActionResult DeleteUser(int IdUser)
        {
            ViewBag.Session = HttpContext.Session.GetString("Code");
            if (ViewBag.Session == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                ViewBag.Session = HttpContext.Session.GetString("Code");

                string ConnectionString = _configuration.GetConnectionString("DBConnectString");
                SqlConnection connection = new SqlConnection(ConnectionString);
                SqlCommand command = connection.CreateCommand();

                connection.Open();
                string SqlQuery = "exec GetDataEdit @ID";

                command.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter(SqlQuery, connection);
                da.SelectCommand.Parameters.AddWithValue("@ID", Convert.ToInt32(IdUser));
                DataTable dataTable = new DataTable();

                int UserDetail = da.Fill(dataTable);

                var user = new User();
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    user.Id = Convert.ToInt32(dataTable.Rows[i]["Id"]);
                }
                return View("DeleteUser", user);
            }
        }

        [HttpPost]
        public IActionResult DeleteUser(User user)
        {
            ViewBag.Session = HttpContext.Session.GetString("Code");
            if (ViewBag.Session == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                try
                {
                    string ConnectionString = _configuration.GetConnectionString("DBConnectString");
                    SqlConnection connection = new SqlConnection(ConnectionString);
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "TimUser";
                    cmd.Parameters.AddWithValue("@ID", user.Id);
                    int UserRecord = Convert.ToInt32(cmd.ExecuteScalar());

                    if (UserRecord == 1)
                    {
                        SqlCommand cmd1 = connection.CreateCommand();
                        cmd1.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd1.CommandText = "delete_user";
                        cmd1.Parameters.AddWithValue("@ID", user.Id);
                        cmd1.ExecuteScalar();
                        ViewBag.DeleteUser = "Xóa thành công " + user.Code;
                        return View("DeleteUser", user);
                    }
                    else
                    {
                        ViewBag.DeleteUsreErro = "Không tim thấy User có ID:" + user.Id + " để xóa!";
                        return View("DeleteUser", user);
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.DeleteUsreErro = "Không tồn tại User có ID:" + user.Id + " để xóa!";
                    return View("DeleteUser", user);
                }
            }
        }

        public DataTable ConvertToDataTable<T>(List<T> models)
        {

            DataTable dataTable = new DataTable(typeof(T).Name);
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in models)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;
        }

        private static void DataSetToExcel(DataSet dataSet, string filePath)
        {
            using (ExcelPackage pck = new ExcelPackage())
            {
                foreach (DataTable dataTable in dataSet.Tables)
                {
                    ExcelWorksheet workSheet = pck.Workbook.Worksheets.Add(dataTable.TableName);
                    workSheet.Cells["A1"].LoadFromDataTable(dataTable, true);
                }

                pck.SaveAs(new FileInfo(filePath));
            }
        }

        public void ExportExcel()
        {
            string ConnectionString = _configuration.GetConnectionString("DBConnectString");
            SqlConnection connection = new SqlConnection(ConnectionString);
            connection.Open();
            string SqlQuery = "exec GetUser";
            SqlCommand command = connection.CreateCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(SqlQuery, connection);
            DataTable dataTable = new DataTable();

            da.Fill(dataTable);
            FileInfo newFile = new FileInfo(@"F:\CTYCUONGKHANG\NetCore\QL_USERNetCore\QL_USERNetCore\import\UserDetail.xlsx");

            if (newFile.Exists)
            {
                newFile.Delete();  // ensures we create a new workbook
                newFile = new FileInfo(@"F:\CTYCUONGKHANG\NetCore\QL_USERNetCore\QL_USERNetCore\import\UserDetail.xlsx");
            }
           
            using (ExcelPackage pck = new ExcelPackage(newFile))
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("UserDetail");
                ws.Cells["A1"].LoadFromDataTable(dataTable, true);
                pck.Save();
            }        
        }
        public FileResult Download(string file)
        {
           
            byte[] fileBytes = System.IO.File.ReadAllBytes(@"F:\CTYCUONGKHANG\NetCore\QL_USERNetCore\QL_USERNetCore\import\" + file);
            var response = new FileContentResult(fileBytes, "application /octet-stream");
            response.FileDownloadName = "UserDetail.xlsx";
            return response;
        }
    }
}
