﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Session;
using Microsoft.AspNetCore.Http;

namespace QL_USERNetCore.Controllers
{
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Session;
    using Microsoft.Data.SqlClient;
    using Microsoft.EntityFrameworkCore.Infrastructure;
    using QL_USERNetCore.Models;
    using System.Data;
    using System.Security.Cryptography.X509Certificates;

   
    public class LoginController : Controller
    {
        //DB_USERContext dB_USERContext = new DB_USERContext();
        string sqlconnectStr = "Server=DESKTOP-RIB17HR\\SQLEXPRESS;Database=DB_USER;;User ID=sa;Password=sa123";
        private readonly DB_USERContext dB_USERContext;
        public LoginController(DB_USERContext context)
        {
            this.dB_USERContext = context;
        }
        public IActionResult Index()
        {
            return View();
        }
    
        public IActionResult Autherise(QL_USERNetCore.Models.User models)
        {
            
            SqlConnection connection = new SqlConnection(sqlconnectStr);
            connection.Open();
            SqlCommand cmd = connection.CreateCommand();

            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "CheckLogin";

            cmd.Parameters.AddWithValue("@Code", models.Code);
            cmd.Parameters.AddWithValue("@Password", models.Password);

            var LoginResult = cmd.ExecuteScalar().ToString();

            if (int.Parse(LoginResult) == 0)
            {
                return View("Index", models);
            }
            else
            {
                SqlConnection connection1 = new SqlConnection(sqlconnectStr);
                connection1.Open();
                SqlCommand cmd1 = connection1.CreateCommand();
                //Luu History
                cmd1.CommandType = System.Data.CommandType.StoredProcedure;
                cmd1.CommandText = "SaveHistoryLogin";

                cmd1.Parameters.AddWithValue("@Code", models.Code);
                cmd1.Parameters.AddWithValue("@LoginDate ", DateTime.Now);

                cmd1.ExecuteScalar();

                HttpContext.Session.SetString("Code", models.Code);
                return RedirectToAction("Index", "Home");
            }
        }

        public IActionResult LogOut()
        {
            var code = HttpContext.Session.GetString("Code").ToString();

            //Luu History
            SqlConnection connection = new SqlConnection(sqlconnectStr);
            connection.Open();
            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "SaveHistoryLogOut";

            cmd.Parameters.AddWithValue("@Code", code);
            cmd.Parameters.AddWithValue("@LogOutDate ", DateTime.Now);

            cmd.ExecuteScalar();

            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Login");
        }
    }
}
