﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Threading.Tasks;
using Excel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Reflection;
using OfficeOpenXml;
using QL_USERNetCore.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using Microsoft.IdentityModel.Protocols;
using System.Configuration;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Math;
using DocumentFormat.OpenXml.Vml.Spreadsheet;
using System.Data.Common;
using System.Linq;

namespace QL_USERNetCore.Controllers
{
    public class ImportExcelController : Controller
    {
        private readonly DB_USERContext dB_USERContext;
        public ImportExcelController(DB_USERContext context)
        {
            this.dB_USERContext = context;
        }

        public IActionResult Index()
        {
            try
            {
                ViewBag.Session = HttpContext.Session.GetString("Code");

                if (ViewBag.Session == null)
                {
                    return RedirectToAction("Index", "Login");
                }
                else
                {
                    ViewBag.Session = HttpContext.Session.GetString("Code");
                return View();
                }
            } catch (Exception e)
            {
                return View();
            }

        }

        [HttpPost]
        public async Task<IActionResult> Index(IFormFile file)
        {
            ViewBag.Session = HttpContext.Session.GetString("Code");

            //if (ViewBag.Session == null)
            //{
            //    return RedirectToAction("Index", "Login");
            //}
            //else
            //{
                if (file == null || file.Length == 0)
                    return Content("file not selected");

                var path = Path.Combine(
                            Directory.GetCurrentDirectory(), @"F:\CTYCUONGKHANG\NetCore\QL_USERNetCore\QL_USERNetCore\import\",
                            file.FileName);

                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }

                ImportData(file.FileName, path);
                return View();
            //}
        }

        public static DataTable ReadExcel(string FileName, string path)
        {
            DataSet result = new DataSet();
            try
            {
                string Extension = Path.GetExtension(FileName);

                FileStream stream = System.IO.File.Open(path, FileMode.Open, FileAccess.Read);
                if (Extension == ".xls")
                {
                    //for excel 2003
                    IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(stream);

                    excelReader.IsFirstRowAsColumnNames = true;
                    result = excelReader.AsDataSet();
                    return result.Tables[0];
                }
                else
                {
                    // for Excel 2007
                    IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    //excelReader.IsClosed();
                    excelReader.IsFirstRowAsColumnNames = true;
                    result = excelReader.AsDataSet();

                    return result.Tables[0];                   
                }
            }
            catch (Exception ex) { return null; }
        }

        static DataTable ConvertToDataTable<T>(List<T> models)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in models)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;
        }

        public string ConvertDatatableToXML(DataTable dt)
        {
            StringWriter swStringWriter = new StringWriter();
            dt.WriteXml(swStringWriter);
            //Datatable as XML string
            return swStringWriter.ToString();
        }
        public void exportToExcel(DataTable dt, string path)
        {
            try
            {
                FileInfo newFile = new FileInfo(@"F:\CTYCUONGKHANG\NetCore\QL_USERNetCore\QL_USERNetCore\import\ErrorTMP.xls");

                if (newFile.Exists)
                {
                    newFile.Delete();  // ensures we create a new workbook
                    newFile = new FileInfo(@"F:\CTYCUONGKHANG\NetCore\QL_USERNetCore\QL_USERNetCore\import\ErrorTMP.xls");
                }

                using (ExcelPackage pck = new ExcelPackage(newFile))
                {
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("ErrorTMP");
                    ws.Cells["A1"].LoadFromDataTable(dt, true);
                    pck.Save();
                }
            }
            catch (Exception ex)
            {
                //Debug("error: " + ex.Message);//catch nothing
            }

        }

        public class Data
        {
            public string Code { get; set; }
            public string Name { get; set; }    
            public string Passwprd { get; set; }
            public string Loi { get; set; }
        }
        public void ImportData(string fileName, string path)
        {
            DataTable dtb = ReadExcel(fileName, path);

            int totalLine = dtb.Rows.Count;

            dtb.TableName = "User";
            dtb.Columns[0].ColumnName = "Code";
            dtb.Columns[1].ColumnName = "Name";
            dtb.Columns[2].ColumnName = "Password";
            string xmlString = string.Empty;
            xmlString = ConvertDatatableToXML(dtb);
          


            string sqlconnectStr = "Server=DESKTOP-RIB17HR\\SQLEXPRESS;Database=DB_USER;;User ID=sa;Password=sa123";
            SqlConnection connection = new SqlConnection(sqlconnectStr);
            connection.Open();
            string StrQuery = "exec ProcUser @Usr, @Data, @Namespace";
            using (SqlCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = StrQuery;
                cmd.Parameters.AddWithValue("@Usr", HttpContext.Session.GetString("Code"));
                cmd.Parameters.AddWithValue("@Data", xmlString);
                cmd.Parameters.AddWithValue("@Namespace", "NewDataSet/User");

                SqlDataAdapter da = new SqlDataAdapter(StrQuery, connection);

                da.SelectCommand.Parameters.AddWithValue("@Usr", "admin");
                da.SelectCommand.Parameters.AddWithValue("@Data", xmlString);
                da.SelectCommand.Parameters.AddWithValue("@Namespace", "NewDataSet/User");
                DataTable dataTable = new DataTable();
                var t = da.Fill(dataTable);
                if(t > 0)
                {
                    exportToExcel(dataTable, path);
                    TempData["KhongLoi"] = "File import khong loi";
                }
                
                int thanhcong = totalLine - t;
                ViewBag.ImportNofication = "Số dòng thành công:  " + thanhcong + ".Số dòng thất bại: " + t +"." ;
                TempData["ImportSuccess"] = "Import thành công!";
               
            }

        }

        public FileResult Download(string file)
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(@"F:\CTYCUONGKHANG\NetCore\QL_USERNetCore\QL_USERNetCore\import\" + file);
            var response = new FileContentResult(fileBytes, "application /octet-stream");
            response.FileDownloadName = "ErroImportDetails.xlsx";
            return response;
        }

    }
}
