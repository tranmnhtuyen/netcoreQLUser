﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;


namespace QL_USERNetCore.Controllers
{
    using X.PagedList;
    using QL_USERNetCore.Models;
    using OfficeOpenXml;
    using System.Data;
    using System.Reflection;
    using ClosedXML.Excel;
    using System.IO;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Data.SqlClient;

    public class HistoryLoginController : Controller
    {
       
        private readonly DB_USERContext dB_USERContext;

        private readonly IConfiguration _configuration;
        public HistoryLoginController(IConfiguration configuration)
        {
            _configuration = configuration;

        }
        // GET: HistoryLogin
        public ActionResult Index(int? page)
        {
            ViewBag.Session = HttpContext.Session.GetString("Code");
            if (ViewBag.Session == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                var n = HttpContext.Session.GetString("Code").ToString();

                // 1. Tham số int? dùng để thể hiện null và kiểu int
                // page có thể có giá trị là     và kiểu int.

                // 2. Nếu page = null thì đặt lại là 1.
                if (page == null) page = 1;


                string ConnectionString = _configuration.GetConnectionString("DBConnectString");
                SqlConnection connection = new SqlConnection(ConnectionString);
                connection.Open();
                string SqlQuery = "exec GetHistory";
                SqlCommand command = connection.CreateCommand();
                command.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter(SqlQuery, connection);
                DataTable dataTable = new DataTable();

                int link = da.Fill(dataTable);

                List<HistoryLogin> HistoryList = new List<HistoryLogin>();

                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    HistoryLogin history = new HistoryLogin();
                    history.Id = Convert.ToInt32(dataTable.Rows[i]["ID"]);
                    history.Code = dataTable.Rows[i]["Code"].ToString();
                    history.LoginDate = dataTable.Rows[i]["LoginDate"].ToString();
                    history.LogoutDate = dataTable.Rows[i]["LogoutDate"].ToString();
                    HistoryList.Add(history);
                }
                int pageSize = 10;

                var links = HistoryList.ToPagedList(page ?? 1, pageSize);


                // 4.1 Toán tử ?? trong C# mô tả nếu page khác null thì lấy giá trị page, còn
                // nếu page = null thì lấy giá trị 1 cho biến pageNumber.
                int pageNumber = (page ?? 1);

                // 5. Trả về các Link được phân trang theo kích thước và số trang.
                return View(links.ToPagedList(pageNumber, pageSize));
            }

        }
        public ActionResult DisplayHistoy()
        {
            var n = HttpContext.Session.GetString("Code").ToString();
            //var n = Session["Code"];
            //var user = db.Users.ToList();
            var HistoryRecords = dB_USERContext.HistoryLogin.Where(p => p.Code == n).ToList();
            return View("Index", HistoryRecords);
        }
    }
}
